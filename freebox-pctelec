#!/usr/bin/env python3

import itertools, enum, collections.abc
from remotefreebox.freeboxcontroller import FreeboxController

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
from gi.repository import Gtk, Gdk




class CyclicEnum(enum.Enum):
    def __init__(self, *_):
        real_members = list(self.__class__)
        if real_members:
            self._prev = real_members[-1]
            self._prev._next = self
            self._next = real_members[0]
            self._next._prev = self
        else:
            self._prev = self._next = self
    @property
    def next(self):
        return self._next
    @property
    def prev(self):
        return self._prev

class KeyMap(collections.abc.MutableMapping):
    def __init__(self, *args, **kwds):
        self._modifiers = []
        self._keymap = {}
        self.update(*args, **kwds)

    def _expand_key(self, key):
        try:
            _, _ = key
            return key
        except TypeError:
            return (None, key)

    def __getitem__(self, key):
        modifier,keyname = self._expand_key(key)
        try:
            return self._keymap[modifier][keyname]
        except KeyError:
            raise KeyError(key)

    def __delitem__(self, key):
        modifier,keyname = self._expand_key(key)
        try:
            del self._keymap[modifier][keyname]
        except KeyError:
            raise KeyError(key)

    def __setitem__(self, key, item):
        modifier,keyname = self._expand_key(key)
        try:
            d = self._keymap[modifier]
        except KeyError:
            d = self._keymap[modifier] = {}
            if modifier is not None:
                self._modifiers.append(modifier)
                self._modifiers.sort(reverse = True, key=int)
        d[keyname] = item

    def __iter__(self):
        for modifier in self._modifiers:
            for key in self._keymap[modifier]:
                yield (modifier, key)
        for key in self._keymap[None]:
            yield key

    def __len__(self):
        return sum((len(d) for d in self._keymap.values()))

    def lookup(self, event):
        for modifier in self._modifiers:
            if event.state & modifier == modifier:
                d = self._keymap[modifier]
                if event.keyval in d:
                    return d[event.keyval]
        try:
            return self._keymap[None][event.keyval]
        except KeyError:
            raise KeyError

ALLMODES_KEYS = KeyMap({
        Gdk.KEY_Left: "Left",
        Gdk.KEY_Up: "Up",
        Gdk.KEY_Down: "Down",
        Gdk.KEY_Right: "Right",
        Gdk.KEY_Return: "Enter",
        Gdk.KEY_BackSpace: "Backspace",
        (Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK,
            Gdk.KEY_Escape): "Power",
        (Gdk.ModifierType.CONTROL_MASK, Gdk.KEY_Escape): "Free",
        Gdk.KEY_Escape: "Back"
})

for i in range(10): print()

KEYBOARD_KEYS = KeyMap({
        Gdk.KEY_Tab: "Tab",
})


COMMAND_KEYS = KeyMap({
        Gdk.KEY_KP_Add: "Vol+",
        Gdk.KEY_KP_Subtract: "Vol-",
        Gdk.KEY_Page_Up: "Chan+",
        Gdk.KEY_Page_Down: "Chan-",
        Gdk.KEY_Home: "Rewind",
        Gdk.KEY_End: "Fast Forward",
        Gdk.KEY_space: "Play/Pause",
})

COMMAND_KEYS.update(
        ((getattr(Gdk, "KEY_KP_{0}".format(i)), str(i)) for i in range(10))
)
COMMAND_KEYS.update(
        ((getattr(Gdk, "KEY_{0}".format(i)), str(i)) for i in range(10))
)

print(list(ALLMODES_KEYS.items()))

class SendMode(CyclicEnum):
    Keyboard = KEYBOARD_KEYS
    Command = COMMAND_KEYS

#### Commands not mapped yet ####
#    # Remote control emulation
#    "AV",
#    "Search",
#    "Menu",
#    "Info",
#    "Mute",
#    "Record",
#
#    # Additional keys
#    "Stream debug",
#    "Stop",
#    "Play",
#    "Random Play",
#    "Previous track",
#    "Next track",
#    "Zoom+",
#    "Zoom-",
#    "Browser Back",
#    "Browser Forward",
#    "Browser Refresh",
#    "Browser Stop",
#    "Video Track",
#    "Audio Track",
#    "Subtitle Track",
#    "System Sleep",
#    "System Wakeup",
#    "Eject",
#    "Tab",
#    "Power/kbd",
#    "Context Menu",
#    "AL Internet Browser",


class MyWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self)

        self.resize(1200, 400)

        self.connect("delete-event", Gtk.main_quit)
        self.connect("key-press-event", self.on_key)

        #drawing = Gtk.DrawingArea()
        #drawing.connect("draw", self.on_draw)
        #self.add(drawing)

        self.control = FreeboxController()
        self.set_mode(SendMode.Keyboard)

    def set_mode(self, mode = None):
        if isinstance(mode, SendMode):
            self.mode = mode
        else:
            self.mode = self.mode.next
        self.set_title(self.mode.name)

    def lookup(self, event, keylist):
        try:
            action = keylist.lookup(event)
        except KeyError:
            return False
        self.control.press(action)
        return True

    def on_key(self, _, event):

        if event.keyval == Gdk.KEY_Tab and \
                event.state & Gdk.ModifierType.CONTROL_MASK:
            self.set_mode()
        elif self.lookup(event, ALLMODES_KEYS):
            pass
        elif self.lookup(event, self.mode.value):
            pass
        elif self.mode is SendMode.Keyboard:
            self.control.write(event.string)
        else:
            print("-- Key not mapped:")
            for key in dir(Gdk):
                if key.startswith("KEY_") and \
                        event.keyval == getattr(Gdk, key):
                    print("  keyval = {0}".format(key))
            print("  mask = {0}".format(" | ".join(
                mask for mask in dir(Gdk.ModifierType)
                     if mask.endswith("_MASK") and
                        event.state & getattr(Gdk.ModifierType, mask)
            )))

        return True

win = MyWindow()
win.show_all()
Gtk.main()
